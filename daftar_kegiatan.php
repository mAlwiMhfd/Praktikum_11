<?php
    include_once 'top.php';
    require_once 'class_kegiatan.php';
    //masukkan file untuk memanupilasi data
?>
<h2>DAFTAR KEGIATAN</h2>
<?php
    $obj_kegiatan = new Kegiatan();
    $rows = $obj_kegiatan -> getAll();
    //buat variabel untuk mendapat semua isi tabel
?>
<table class="table">
    <thread>
    <tr class="active">
    <th>No</th>
    <th>Kode</th>
    <th>Judul Kegiatan</th>
    <th>Narasumber</th>
    <th>Action</th>
    </tr>
    </thread>
    <tbody>
    <?php
        $nomor=1;
        foreach($rows as $row){
            echo '<tr><td>'.$nomor.'</td';
            echo '<td>'.$row['kode'].'</td>';
            echo '<td>'.$row['judul'].'</td>';
            echo '<td>'.$row['narasumber'].'</td>';
            echo '<td><a href="view_kegiatan.php?id'.$row['id']. '">View</a> |';
            echo '<a href="form_kegiatan.php?id'.$row['id']. '">Update<a/></td';
            echo '</tr>';
            $nomor++;
        }
    ?>
</table>
<!--Buat tabel untuk menampilkan data-->
<?php
    include_once 'bottom.php';
?>